#Bicing Barcelona 2018#

![Bicing Barcelona 2018](https://bitbucket.org/davidtrigo/bicing-barcelona-2018/raw/c452791ac43db181c23fb5f1bf2dea232ec942cf/img/log-bicing.png "Bicing Barcelona 2018")

Aplicaci�n que muestra las estaciones de bicing m�s cercanas a una posici�n dada dando uso del [API p�blico](http://opendataajuntament.barcelona.cat/data/es/dataset/bicing "API p�blico")


####Pantalla 1

Esta pantalla contiene:

1. Un buscador donde podremos especificar las coordenadas (longitud y latitud) de b�squeda y la distancia m�xima de las estaciones que queremos que aparezcan en el  resultado de la b�squeda. Para calcular la distancia entre dos puntos se ha hecho la siguiente [funci�n]( https://www.taringa.net/posts/hazlo-tu-mismo/14270601/PHP-Calculardistancia-entre-dos-coordenadas.html "funci�n Calcular dsitancia entre dos coordenadas")


2. Si se ha hecho una b�squeda, el resultado de la b�squeda (tipo de estaci�n y nombre de la
calle) en forma de lista. Cada elemento de la lista ser� un enlace a la informaci�n 
completa del recurso.


####Pantalla 2

Esta pantalla incluye el buscador de la pantalla 1 y toda la informaci�n de la estaci�n seleccionada. Tambi�n incluye una lista (tipo de estaci�n y nombre de la calle) de las estaciones  m�s cercanas. Cada elemento de la lista ser� un enlace a la informaci�n completa del recurso.

---

Proyecto realizado en el curso de Desarrollo de aplicaciones con tecnolog�a web en el m�dulo 3 Programaci�n web entorno servidor de la unidad formativa Desarrollo de aplicaciones web distribuidas.


A d�a de hoy, el dataset utilizado es anterior al actual por lo que no mostrar�  ninguna estaci�n de a la seleccionada.


